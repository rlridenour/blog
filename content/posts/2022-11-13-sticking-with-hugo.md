+++
title = "Sticking with Hugo"
author = ["Randy Ridenour"]
date = 2022-11-13T00:00:00-06:00
lastmod = 2023-01-21T11:01:33-06:00
tags = ["org"]
draft = false
+++

I have been toying with the idea of blogging entirely with Emacs and Org Mode, but I haven't been able to make it work like I like. I guess I'll stick with Hugo. 

