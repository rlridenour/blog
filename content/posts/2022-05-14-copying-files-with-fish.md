+++
title = "Copying Files with Fish"
author = ["Randy Ridenour"]
tags = ["fish"]
draft = false
+++

I needed to copy certain files from a bunch of subfolders to another folder. This turned out to be an easy way to do in Fish shell. 

```shell
cp (find . -name '<name>') <target folder>
```
