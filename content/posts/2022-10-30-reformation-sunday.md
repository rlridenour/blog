+++
title = "Reformation Sunday"
author = ["Randy Ridenour"]
date = 2022-10-30T00:00:00-05:00
lastmod = 2022-10-30T19:15:36-05:00
tags = ["religion"]
draft = false
+++

We had our annual Reformation Sunday gathering at the local German restaurant. My church doesn't count itself in the Reformed tradition, but we will take any excuse to get together for good food and drink.

