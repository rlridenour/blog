+++
title = "Harmony of the Gospels"
author = ["Randy Ridenour"]
date = 2023-01-21T00:00:00-06:00
lastmod = 2023-01-21T10:46:11-06:00
draft = false
+++

This is a schedule for a study of the Gospels in our Sunday School Class at NorthHaven Church.

| Date  | Subject                                                             | Text                                                         |
|-------|---------------------------------------------------------------------|--------------------------------------------------------------|
| 6/19  | Introduction                                                        |                                                              |
| 6/26  | Jesus' Genealogy                                                    | Matthew 1:1-17 Luke 3:23-38                                  |
| 7/3   | Leaving Egypt and Settling in Nazareth                              | Matthew 2:19-23 Luke 2:39                                    |
| 7/10  | John the Baptist's Ministry                                         | Matthew 3:1-12 Mark 1:1-8 Luke 3:1-18                        |
| 7/17  | The Baptism of Jesus                                                | Matthew 3:13-17 Mark 1:9-11 Luke 3:21-22                     |
| 7/24  | Jesus' Temptation in the Wilderness                                 | Matthew 4:1-11 Mark 1:12-13 Luke 4:1-13                      |
| 7/31  | Jesus' Arrival and Early Ministry in Galilee                        | Matthew 4:12 Mark 1:14-15 Luke 4:14-15 John 4:43-45          |
| 8/7   | Jesus Calls Four Disciples                                          | Matthew 4:18-22 Mark 1:16-20                                 |
| 8/14  | Jesus Authenticates His Teaching in Capernaum by Healing a Demoniac | Mark 1:21-28 Luke 4:31-37                                    |
| 8/21  | Jesus Heals Peter's Mother-in-Law and Others                        | Matthew 8:14-17 Mark 1:29-34 Luke 4:38-41                    |
| 8/28  | Jesus' Preaching Tour of Galilee with Simon and Others              | Matthew 4:23-25 Mark 1:35-39 Luke 4:42-44                    |
| 8/28  | A Leper Healed by Jesus                                             | Matthew 8:1-4 Mark 1:40-45 Luke 5:12-16                      |
| 9/4   | A Paralytic is Forgiven and Healed                                  | Matthew 9:1-8 Mark 2:1-12 Luke 5:17-26                       |
| 9/11  | Levi/Matthew is Called to be a Disciple                             | Matthew 9:9-13 Mark 2:13-17 Luke 5:27-32                     |
| 9/18  | Jesus is Questioned About Fasting                                   | Matthew 9:14-17 Mark 2:18-22 Luke 5:33-39                    |
| 9/25  | Jesus' Disciples Pluck Grain on a Sabbath                           | Matthew 12:1-8 Mark 2:23-28 Luke 6:1-5                       |
| 10/2  | Jesus Heals a Man's Withered Hand on a Sabbath                      | Matthew 12:9-14 Mark 3:1-6 Luke 6:6-11                       |
| 10/9  | Jesus' Popularity Grows                                             | Matthew 12:15-21 Mark 3:7-12                                 |
| 10/16 | The Twelve Disciples Chosen and Named                               | Mark 3:13-19 Luke 6:12-16                                    |
| 10/23 | Characteristics of the Godly                                        | Matthew 5:1-16 Luke 6:17-26                                  |
| 10/30 | Jesus Practically Applies the Meaning of the Law                    | Matthew 5:21-48 Luke 6:27-36                                 |
| 11/6  | Wise Words on Judging                                               | Matthew 7:1-6 Luke 6:37-42                                   |
| 11/13 | Jesus' Words on Righteous Living                                    | Matthew 7:13-27 Luke 6:43-49                                 |
| 11/20 | The Centurion's Servant Healed                                      | Matthew 8:5-13 Luke 7:1-10                                   |
| 11/27 | Messages From and About John the Baptist                            | Matthew 11:2-30 Luke 7:18-35                                 |
| 12/4  | Jesus Responds to Accusations of Casting Out Demons by Beelzebub    | Matthew 12:22-45 Mark 3:20-30                                |
| 12/11 | Jesus' True Family is Spiritual                                     | Matthew 12:46-50 Mark 3:31-35 Luke 8:19-21                   |
| 12/18 | The Parable of the Soils                                            | Matthew 13:1-9 Mark 4:1-9 Luke 8:4-8                         |
| 1/8   | The Reason for the Parables                                         | Matthew 13:10-17 Mark 4:10-12 Luke 8:9-10                    |
| 1/15  | Parable of the Soils Explained                                      | Matthew 13:18-23 Mark 4:13-20 Luke 8:11-15                   |
| 1/22  | Jesus Speaks on the Use of Parables                                 | Mark 4:21-25 Luke 8:16-18                                    |
| 1/29  | The Parable of the Mustard Seed and Tree                            | Matthew 13:31-32 Mark 4:30-32                                |
| 2/5   | Jesus' Custom of Speaking in Parables                               | Matthew 13:34-35 Mark 4:33-34                                |
| 2/12  | Jesus Calms the Storm                                               | Matthew 8:18-27 Mark 4:35-41 Luke 8:22-25                    |
| 2/19  | Healing the Gerasene Demoniacs                                      | Matthew 8:28-34 Mark 5:1-20 Luke 8:26-39                     |
| 2/26  | The Raising of Jairus' Daughter, and Other Acts of Healing          | Matthew 9:18-34 Mark 5:21-43 Luke 8:40-56                    |
|       | A Final Visit to Unbelieving Nazareth                               | Matthew 13:54-58 Mark 6:1-6                                  |
|       | Shortage of Laborers                                                | Matthew 9:35-38 Mark 6:6                                     |
|       | Jesus Sends Forth the Twelve for Ministry                           | Matthew 10:1-42 Mark 6:7-11 Luke 9:1-5                       |
|       | The Departure of Jesus and the Disciples                            | Matthew 11:1 Mark 6:12-13 Luke 9:6                           |
|       | The Death of John the Baptist                                       | Matthew 14:1-12 Mark 6:14-29 Luke 9:7-9                      |
|       | Return of the Disciples                                             | Mark 6:30 Luke 9:10                                          |
|       | Jesus Feeds Five Thousand                                           | Matthew 14:13-21 Mark 6:31-44 Luke 9:10-17 John 6:1-15       |
|       | Jesus Walks on the Sea                                              | Matthew 14:22-33 Mark 6:45-52 John 6:16-21                   |
|       | Healings at Gennesaret                                              | Matthew 14:34-36 Mark 6:53-56                                |
|       | Traditions and Commandments                                         | Matthew 15:1-20 Mark 7:1-23 John 7:1                         |
|       | The Syrophoenician Woman's Faith                                    | Matthew 15:21-28 Mark 7:24-30                                |
|       | Jesus Returns to the Sea of Galilee                                 | Matthew 15:29-31 Mark 7:31-37                                |
|       | Jesus Feeds Four Thousand                                           | Matthew 15:32-39 Mark 8:1-10                                 |
|       | Pharisees Demand a Sign from Jesus                                  | Matthew 16:1-12 Mark 8:11-21                                 |
|       | Peter's Confession of Jesus as the Christ                           | Matthew 16:13-20 Mark 8:27-30 Luke 9:18-21                   |
|       | Jesus Foretells His Death                                           | Matthew 16:21-28 Mark 8:31-9:1 Luke 9:22-27                  |
|       | The Transfiguration                                                 | Matthew 17:1-13 Mark 9:2-13 Luke 9:28-36                     |
|       | Healing of a Demoniac Boy                                           | Matthew 17:14-21 Mark 9:14-29 Luke 9:37-43                   |
|       | Jesus Foretells His Death Again                                     | Matthew 17:22-23 Mark 9:30-32 Luke 9:43-45                   |
|       | Who is Greatest in God's Kingdom?                                   | Matthew 18:1-5 Mark 9:33-37 Luke 9:46-48                     |
|       | Warning Against Being a Stumbling Block                             | Matthew 18:6-14 Mark 9:38-50 Luke 9:49-50                    |
|       | The Departure from Galilee                                          | Matthew 19:1-2 Mark 10:1 Luke 9:51-56 John 7:10              |
|       | Jesus' Teaching on Divorce                                          | Matthew 19:3-12 Mark 10:2-12                                 |
|       | Jesus Blesses the Little Children                                   | Matthew 19:13-15 Mark 10:13-16 Luke 18:15-17                 |
|       | The Rich Young Man                                                  | Matthew 19:16-30 Mark 10:17-31 Luke 18:18-30                 |
|       | Jesus Predicts His Death Again                                      | Matthew 20:17-19 Mark 10:32-34 Luke 18:31-34                 |
|       | Warning Against Ambitious Pride                                     | Matthew 20:20-28 Mark 10:35-45                               |
|       | Blind Bartimaeus and Companion Healed                               | Matthew 20:29-34 Mark 10:46-52 Luke 18:35-43                 |
|       | Mary's Anointing of Jesus for Burial                                | Matthew 26:6-13 Mark 14:3-9 John 12:2-8                      |
|       | The Triumphal Entry                                                 | Matthew 21:1-11 Mark 11:1-11 Luke 19:29-44 John 12:12-19     |
|       | The Cursing of the Fig Tree                                         | Matthew 21:18-19 Mark 11:12-14                               |
|       | Second Cleansing of the Temple                                      | Matthew 21:12-17 Mark 11:15-19 Luke 19:45-48                 |
|       | The Lesson of the Withered Fig Tree                                 | Matthew 21:18-22 Mark 11:20-25                               |
|       | Jesus' Authority Challenged                                         | Matthew 21:23-27 Mark 11:27-33 Luke 20:1-8                   |
|       | Parable of the Tenants                                              | Matthew 21:33-46 Mark 12:1-12 Luke 20:9-19                   |
|       | Paying Taxes to Caesar                                              | Matthew 22:15-22 Mark 12:13-17 Luke 20:20-26                 |
|       | Marriage and the Resurrection                                       | Matthew 22:23-33 Mark 12:18-27 Luke 20:27-38                 |
|       | The Greatest Commandment                                            | Matthew 22:34-40 Mark 12:28-34 Luke 20:39-40                 |
|       | Question About the Son of David                                     | Matthew 22:41-46 Mark 12:35-37 Luke 20:41-44                 |
|       | Listen to the Pharisees, but Don't Follow Their Deeds               | Matthew 23:1-12 Mark 12:38-40 Luke 20:45-47                  |
|       | The Poor Widow's Offering                                           | Mark 12:41-44 Luke 21:1-4                                    |
|       | Destruction of the Temple Foretold by Jesus                         | Matthew 24:1-2 Mark 13:1-2 Luke 21:5-6                       |
|       | Signs of the End of the Age                                         | Matthew 24:3-14 Mark 13:3-13 Luke 21:7-19                    |
|       | The Abomination of Desolation                                       | Matthew 24:15-28 Mark 13:14-23 Luke 21:20-24                 |
|       | The Coming of the Son of Man                                        | Matthew 24:29-31 Mark 13:24-27 Luke 21:25-28                 |
|       | The Unknown Day and Hour                                            | Matthew 24:32-44 Mark 13:28-37 Luke 21:29-33                 |
|       | The Faithful or the Unfaithful Slave                                | Matthew 24:45-51 Luke 21:34-36                               |
|       | The Chief Priests Plot to Kill Jesus                                | Matthew 26:1-5 Mark 14:1-2 Luke 22:1-2                       |
|       | Judas Bargains to Betray Jesus                                      | Matthew 26:14-16 Mark 14:10-11 Luke 22:3-6                   |
|       | Passover Meal Preparation                                           | Matthew 26:17-19 Mark 14:12-16 Luke 22:7-13                  |
|       | Beginning of the Passover Meal                                      | Matthew 26:20 Mark 14:17 Luke 22:14-16                       |
|       | Jesus' Betrayer Identified                                          | Matthew 26:21-25 Mark 14:18-21 Luke 22:21-23 John 13:21-30   |
|       | Jesus Predicts Peter's Denial                                       | Luke 22:31-38 John 13:31-38                                  |
|       | Lord's Supper Instituted                                            | Matthew 26:26-29 Mark 14:22-25 Luke 22:17-20                 |
|       | Second Prediction of Peter's Denial                                 | Matthew 26:30-35 Mark 14:26-31                               |
|       | Jesus' Prayer in Gethsemane                                         | Matthew 26:36-46 Mark 14:32-42 Luke 22:39-46                 |
|       | The Betrayal and Arrest                                             | Matthew 26:47-56 Mark 14:43-52 Luke 22:47-53 John 18:1-12    |
|       | Jesus' Trial Before the Sanhedrin                                   | Matthew 26:57-68 Mark 14:53-65 Luke 22:54                    |
|       | Peter Denies Jesus                                                  | Matthew 26:69-75 Mark 14:66-72 Luke 22:55-62 John 18:25-27   |
|       | Jesus Still Before the Sanhedrin                                    | Matthew 27:1 Mark 15:1 Luke 22:63-71                         |
|       | Jesus Before Pilate                                                 | Matthew 27:2, 11-14 Mark 15:1-5 Luke 23:1-5 John 18:28-38    |
|       | Jesus Back Before Pilate                                            | Matthew 27:15-26 Mark 15:6-15 Luke 23:13-25 John 18:39-19:16 |
|       | Roman Soldiers Mock Jesus                                           | Matthew 27:27-31 Mark 15:16-20                               |
|       | The Crucifixion                                                     | Matthew 27:32-56 Mark 15:21-41 Luke 23:26-49 John 19:17-37   |
|       | The Burial of Jesus                                                 | Matthew 27:57-61 Mark 15:42-47 Luke 23:50-56 John 19:38-42   |
|       | The Resurrection Morning                                            | Matthew 28:1-10 Mark 16:1-11 Luke 24:1-12 John 20:1-18       |
|       | The Walk to Emmaus                                                  | Mark 16:12-13 Luke 24:13-35                                  |
|       | Jesus Appears to Ten Disciples                                      | Mark 16:14 Luke 24:36-49 John 20:19-25                       |

