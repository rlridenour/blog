+++
title = "Org Export to PDF with Arara"
author = ["Randy Ridenour"]
date = 2021-07-21T00:00:00-05:00
lastmod = 2021-07-21T16:11:05-05:00
draft = false
+++

I switched from latexmk to [arara](https://mirrors.concertpass.com/tex-archive/support/arara/doc/arara-quickstart.pdf) for compiling \\(\LaTeX\\) files. I like being able to embed the compilation steps into the document. That way, I can run the same command to compile, but use PDFLaTeX on some and LuaLaTeX on others. Org mode's export to PDF uses latexmk, though. I think I managed to set it to arara with this:

```emacs-lisp
(setq org-latex-pdf-process '("arara %f"))
```